---
layout: handbook-page-toc
title: Working with Pods
category: Handling tickets
description: How to work with the Support Pod views on Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Support Pods?

Support Pods is an initiative to introduce subject-specific ticket views and ways that support engineers can work them in Zendesk. This was previously knows as Areas of Focus. History and context behind this can be found in [this epic](https://gitlab.com/groups/gitlab-com/support/-/epics/145).

Note: we are [piloting this with a few support engineers at the moment](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3663), with the intent to roll this out globally within the next few months.

## Why Support Pods?

Working on tickets with the Support Pods view has several positive impacts for both customers and us. Some of them are listed below, in no particular order:

1. Quicker ticket resolution and increased quality of support for customers.
1. Better ticket deflection and representation of customers' voice by improved closer relation with product teams.
1. More opportunities for intentional collaboration on tickets.
1. Focused learning opportunities and clearer sense of progression.

As a support engineer, you might sign up to a Support Pod for one of the following reasons:

- Learning: Support Engineer is looking to up-skill in one or more topics covered by that pod, and can do so by following along specific tickets, collaborating with experts, and deliberately owning tickets in that area to gain knowledge and confidence.
- Helping: Support Engineer is an expert in one or more topics covered by that pod, and can help their peers and customers by engaging on tickets in that pod.

## How can I integrate Support Pods in my day to day?

If you have signed up to a pod, you can use the below prioritization guidelines. This is a slightly modified version of our general [ticket prioritization guidelines](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#how-do-we-prioritize-tickets):

1. New Tickets in the All Regions + Preferred Region. ZD View: “APAC Needs Org/Triage/FRT”
1. Tickets without owners in the All Regions + Preferred Region. ZD View: “APAC+All Regions Needs Assignee”
1. Tickets you own ZD View: “My Assigned Tickets”
1. Support Pod Views
1. Everything else

## Can I sign up to more than 1 pod?

Yes. Since the intent for signing up to a pod can be different (learning vs helping), you might have a need to sign up to more than 1 pod at the same time.

1. Prior to signing up, chat with your manager about it.
1. Create an [issue with Support Ops] to provide you access to the pod view.
1. We recommend signing up to a maximum of 2 pods at a time, 1 for helping and 1 for learning.
    1. This will ensure you do justice to both in terms of time.
    1. You can always sign out of a current pod and sign up to a new pod.

If you are a part of more than one pod and are confused about which pod to begin with, you can use the guidelines below. Note that these are only guidelines, and you should feel free to make a judgement call based on the situation and your needs:

1. Start with the pod(s) that you are a member of to help others.
    1. Think intentional collaboration when you want to help others progress their tickets in the pod.
    1. Add private comments on tickets with your thoughts.
    1. CC yourself on tickets to follow along and assist as needed.
1. Continue to the pod(s) that you are a member of to learn.
    1. Review the tickets in the view.
        1. ZD’s Play option is a good tool to use if you want to skim through the tickets.
        1. Pairing with other pod members is also a good way to learn.
    1. Take assignment of New tickets that you want to work on unless they are High Priority and All Regions.
    1. CC yourself on tickets that you want to follow along and learn from.
    1. There is no expectation to handle Other Preferred Region Tickets from both the New and Open ticket groups.

## Placeholder for current pod groupings

## Placeholder for where to document pod participants

## Placeholder for handovers