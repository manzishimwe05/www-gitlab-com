---
layout: handbook-page-toc
title: "Security Compliance Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i class="fas fa-bullseye" style="color:rgb(110,73,203)" aria-hidden="true"></i> Security Compliance Mission

It is the goal of the GitLab Security Compliance team to:

1. Enable GitLab sales by providing customers information and assurance about our information security program and remove security as a barrier to adoption by our customers.
1. Enable security to scale through the definition of security controls and determining the boundaries and applicability of the information security management system to establish its scope.
1. Work across industries and verticals to support GitLab customers in their own compliance journey.
1. Identify and mitigate GitLab information security risk through continuous control monitoring and automation.

## <i class="far fa-lightbulb" style="color:rgb(110,73,203)" aria-hidden="true"></i> Core Competencies
A member of the [Security Assurance](/handbook/engineering/security/security-assurance/) organization, these are the primary functions of the Security Compliance team:

1. [Internal Security Compliance Audit](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html)
   * [GCF Continuous Control Monitoring](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
   * [GitLab IT General Controls ITGC](/handbook/engineering/security/security-assurance/security-compliance/ITGC/)
   * Continuous Control Monitoring Automation
   * [User Access Reviews](/handbook/engineering/security/security-assurance/security-compliance/access-reviews.html)
   * Business Continuity Plan (BCP) and Information System Continuity (ISCP) testing
1. [Security Certifications](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)
   * External Audits (e.g. SOC 2, SOC 3, ISO, etc.)
   * Gap Assessments/Readiness Assessments
1. [Observation and Remediation](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)
   * Security Control Testing Activities (CCM)
   * Internal Audit Activities
   * Third Party Risk Management (TPRM) Activities
   * Customer Assurance Activities
   * External Audits
   * Third Party Application Scanning
   * Ad-hoc Observations
   * Gap Assessment Activities

## <i id="biz-tech-icons" class="fas fa-tasks"></i>Metrics and Measures of Success

1. [Security Control Health](/handbook/engineering/security/performance-indicators/#security-control-health)
1. [Security Observations](/handbook/engineering/security/performance-indicators/#security-observations-tier-3-risks)

## <i class="fas fa-id-card" style="color:rgb(110,73,203)" aria-hidden="true"></i> Contact the Team

* Slack
   * Feel free to tag is with `@sec-compliance-team`
   * The #sec-assurance slack channel is the best place for questions relating to our team (please add the above tag)
* Tag us in GitLab
   * `@gitlab-com/gl-security/security-assurance/sec-compliance`
* Email
   * `security-compliance@gitlab.com`
* [GitLab Security Compliance team project](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance)

* Interested in joining our team? Check out more [here](https://about.gitlab.com/job-families/engineering/security-compliance/)!

## <i class="fas fa-book" style="color:rgb(110,73,203)" aria-hidden="true"></i> References

* [Security Certifications](/handbook/engineering/security/security-assurance/security-compliance/certifications.html)
* [GCF Security Control Lifecycle](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html)
* [GCF Secrity Controls](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* [User Access Reviews](/handbook/engineering/security/security-assurance/security-compliance/access-reviews.html)
* [Observation Methodology](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html)

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/#" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Security Assurance Homepage</a>
</div> 
