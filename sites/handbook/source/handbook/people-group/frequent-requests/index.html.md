---
layout: handbook-page-toc
title: "Frequently Requested"
description: "Descriptions of frequent requests to the People Group and how to proceed"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Frequent requests that the team often receive may be answered below.  Please take a look before reaching out to the team.

## Team Directory
{: #directory}

GitLab uses Slack profiles as an internal team directory, where team members can add their personal contact details, such as email, phone numbers, or addresses. This is your one-stop directory for phone numbers and addresses (in case you want to send your teammate an awesome card!). Feel free to add your information to your Slack profile (this is completely opt-in!) by clicking on "GitLab" at the top left corner of Slack, "Profile & Account", then "Add Profile" (for the first time making changes) or "Edit Profile" (if your account is already set up) to make any changes!

- Please make sure that your address and phone information are written in such a way that your teammates can reach you from a different country. So, for example, include `+[country code]` in front of your phone number.

## Letter of Employment
{: #letter-of-employment}

If you need a letter from GitLab verifying your employment/contractor status or that you work remotely, please submit this [form](https://docs.google.com/forms/d/e/1FAIpQLSeop7IJJtN9OWasZl9S992sH5iNio0j916SJa0mBDgc2-2hNA/viewform). The letter will  be generated and sent to the team member directly as per this [automation](/handbook/people-group/engineering/miscellaneous/#letter-of-employment). 

The Letter includes the following data points from your BambooHR record; 
- Team Member Name
- GitLab Entity 
- Pay Amount and Frequency 
- Title
- Full Time or Part Time
- Hire Date
- Employment Status
- Probationary Status (if applicable)

If you are a GitLab GmbH employee, please fill out this [form](https://docs.google.com/forms/d/e/1FAIpQLSfiFggqUAA12U_9NV52BLIuuOi7x7oBznIP2iaCcMQYcM39Ag/viewform). The People Experience team will verify the details, and then forward to the team member. 

If you are a GitLab Ltd - UK employee, please fill out this [form](https://docs.google.com/forms/d/e/1FAIpQLSdbWZGR1qtcSx5O9zhi7DmKokC8ww-pOyrqJjpBR69c_jQ_5Q/viewform). The People Experience team will verify the details, and then forward to the team member. 

If you are a PEO employee kindly reach out to the PEO for a letter of employment. If you require internal assistance on this matter please reach out to the People Experience team. 

In addition, if the request comes from a third party, the People Experience team will always verify that the information is appropriate to share and check with you if you have given authorization to share any personal information.

## Employment History Verification (Current Team Members) 

If you need People Operations to complete an employment verification form, usually for the purposes of a loan or security access, please have the relevant company or organization request this by emailing `people-exp@gitlab.com` for Non US team members and `uspayroll@gitlab.com` for US team members. In the event that you are employed via a **PEO** (SafeGuard, Global Upside, CXC. Remote etc) you must request the information directly from them, if you require assistance on the contact details please email `peopleops@gitlab.com`.

The People Experience or Payroll team will complete the requested documents using DocuSign or ensure that the information is sent in an encrypted format. Requests for any information not in BambooHR, the People Experience team will make contact with the relevant department, usually Payroll via private Slack channel.

The People Experience team will always verify and check with you to be sure that you have given authorization to share any personal information, if a signed consent form is not included in the request.

## Employment History Verification (Previous Team Members) 

If former team members need employment verification all requests should be sent to peopleops@gitlab.com.  People Ops will only confirm the following:

- start date
- end date
- job title or positions held

Managers should not complete employment verification for any team members. Instead, they should direct all inquiries to People Ops. Team members can provide personal references following [company guidelines](/handbook/people-group/frequent-requests/#reference-request).

## Request for GitLab's phone number
Team members often are asked for a company phone number to complete a mortgage application, loan request or similar application.  Our company phone number is outlined on the [GitLab Communication page](/handbook/communication/#phone-number) but this phone number will direct the caller to make the request via email.  If the requester requires speaking to a human, please email total-rewards@gitlab and someone on the Total Rewards team will provide their phone number based on availability.

## Reference Request
{: #reference-request}

You do not need permission from GitLab to give a personal reference, but GitLab team members are not authorized to speak on behalf of the company to complete reference requests for GitLab team members no longer working for GitLab. If a team member would like to give a personal reference based on their experience with the former team member, it must be preceded by a statement that the reference is not speaking on behalf of the company. To reinforce this fact, personal references should never be on company letterhead, and telephone references should never be on company time. Remember to always be truthful in reference checks, and instead of giving a majority negative reference, refuse to provide one. Negative references can result in legal action in some jurisdictions.

If an ex team member acted in a way which is against our [Code of Conduct](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d), GitLab may do a company wide request not to provide a reference.

## Paperwork (Werkgeversverklaring) people may need to obtain a mortgage in the Netherlands

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring".
This document describes your salary and states that your employer expects to continue to employ
you after the contract expires (assuming the performance of the team member doesn't degrade).
This document has to be filled in by hand, preferably using blue ink, and must be signed
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed)
must be supplied that states the employer has no stamp. While the language of these
documents doesn't matter, the use of Dutch is preferred.

Team members also have to provide a copy of a payslip that clearly states their
monthly salary and also their annual salary. These numbers must match the numbers on
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require
you to provide information about your financial status, usually in the form of a report/screenshot
of your total financial status (including any savings you have). The requirements for
this seem to vary a bit between mortgage providers.

If you need a werkgeversverklaring, be sure to reach out to `people-connect@gitlab.com`. The People team will then forward the request to our Payroll Vendor in the Netherlands, to ensure the document is created in Dutch.

## Business Cards
{: #business-cards}

Business cards can be ordered through Moo. Please let People Experience team know if you have not received access to your account on Moo by the end of your first week with GitLab. Place the order using your own payment and add it to your expense report. If you need any assistance, let a People Experience team know.

Once you are logged in, follow these steps to create your cards:

1. Select your currency in the upper right corner to ensure that your shipment is sent from the correct location.
1. Select the "+" sign in the upper right corner of your screen. (If you don't see the "+" sign then go to [this URL](https://www.moo.com/mbs/products/business-cards)).
1. Select "Business Cards".
1. Select your template (one has the Twitter & Tanuki symbol and cannot be removed, and one is free of those symbols).
1. Enter your information into the card.
1. Please remember to choose rounded corners.
1. Add the card to your cart and order! We ask for a default of 50 cards unless you are meeting with customers regularly.
1. Add the cards to your expense report under 'office supplies'.

### Business Cards - India

Since MOO ships business cards from outside India via DHL, DHL is mandatorily required to perform "Know Your Customer" (KYC) checks before delivery.
If you are a team member residing in India, please consider using the following tips while ordering business cards from MOO.

- Avoid filling in the "Company name" field when checking out your order to prevent the shipment being sent in the company's name instead of your name. It is only necessary to fill the "First Name" and "Last Name" fields.
- For verification, DHL matches the name and address on your proof of ID with the name and address in your consignment. So, when providing the address for delivery, make sure to provide the same address as on one of your proofs of ID.
- In a scenario where you do not have any ID associated with the address you intend to provide, consider shipping the item in the name of somebody who holds a valid proof of ID at that address, like a relative or a friend.
- Please check out the list of [valid KYC documents](https://dhlindia-kyc.com/forms/valid-kyc-docs.aspx#ind-indian) before placing the order.
- In the event that DHL charges Import Duty for the cards, you may proceed with paying the amount and expensing the amount through your normal expense process.

### Business Cards-Special Characters
-If you are a team member who needs business cards with special characters, please reach out to the People Experience Team (people-connect@gitlab.com)and indicate how you would like the layout to look like.
-The People Experience Coordinator will inform the Moo team to make the template available on Moo. This takes 24-48 hours
-Once the template is available, the People Experience Coordinator will advice you and you can proceed and place your order.

## Japan Daycare Eligibility Documentation

In Japan, for a team member to qualify for subsidized day care, they need to demonstrate that they are working and need a certified document from their employer confirming their employment status.

- Team member: Fill out the daycare documentation with all the necessary details and only leave out the employer bit. Send this document via email to Non US Payroll (nonuspayroll@gitlab.com)
- Non US Payroll: Submit the document to our local payroll provider in Japan for completion
- Local Payroll provider: Send the filled in form back to Non US Payroll
- Non US Payroll: Forward the completed document to the team member 

## Name Change

Our HRIS (human resource management system) is BambooHR and it is our Single Source of Truth for team member personal information, including your [Legal Name](https://en.wikipedia.org/wiki/Legal_name). To initiate a request to have your legal name changed in BambooHR, please complete the following:

- Team member: Open an access request issue following our [handbook instructions](/handbook/business-ops/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request) to request to change your name and all other applicable systems.
- Team member: Email people-connect@gitlab.com, total-rewards@gitlab.com, and payroll@gitlab.com linking your access request issue and including legal documents with proof of your requested new name. Legal documents will be reviewed on a case-by-case basis and People Ops will request additional documentation if warranted. 
    - For US-based team members, an updated Social Security card is preferrable.
       - We ask for an updated Social Security card to insure tax records and payroll are aligned. 
    - For non-US team members, please provide a legal document showing your new name, with the approving regional or federal goverment listed.
- People Experience: Complete all to do steps in the [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/Name_change_request.md)

## Reporting Requests
All requests for BambooHR-related data or reports with personal people data should be made through an [Access Request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests).

1. The Access Request should define exactly what type of personal data is requested, and also define if:
- This will be a one-time request.
- This will be an ongoing request and the requestor needs access to be able to run a report themselves.
1. Please assign all such requests to the People Operations Specialists and ping the team in the comments section of the issue using the `@gl-peopleops` tag.


## Netherlands: Certificate of Good Conduct
1. Once a new team member starts, they will receive an e-mail from an organization called Justis (noreply@justis.nl). This is initiated by HR Savvy.
- The team member needs to follow the steps in this e-mail to request the certificate of good conduct
1. The certificate will then be sent within a couple of weeks to the team member’s home address
1. The team member needs to email their certificate to hr@savvy-group.eu and upload it to BambooHR under the Documents tab, Verification folder
1. Any costs related to this process can get reimbursed by submitting them as an expense. Please follow the process as described on the [expenses handbook page](/handbook/finance/expenses/#-expense-policy).

## Germany: 'Arbeitsbescheinigung' - form sent by Bundesagentur fuer Arbeit
This form is a verification of former employment sent by the Federal Employment Agency in Germany. It is mandatory for employers to fill this form in and currently only available in German language.
- Please email the form to peopleops@gitlab.com and the People Operations Team will work on completing the form. Please allow some time for this, as various information from different stakeholders is required.
- The People Operations Specialist emails the form to nonuspayroll@gitlab.com to fill in salary related information.
- The People Operations Specialist fills in fields using BambooHR and if applicable reaches out to teammemberrelations@gitlab.com with questions relating to the termination.
- Our legal council in Germany can support with questions to make sure the form is filled in correctly. In order for our legal council to review the form, please include the following documents in the email: Employment Contract, Termination letter/agreement and reasons for termination. Please find the contact details in the PeopleOps vault in 1password. 
- As the form requires a company stamp and a wet signature, the People Operations Specialists sends the filled in form by email to our Director of Tax, so it can get signed, stamped and posted to the previous team member. Please include the address the form should get posted to in the email.

## Do you need a Visa Invitation Letter?

If you are **employed via a GitLab entity**, please follow the process below.
   1. Fill out this [form](https://forms.gle/8r934fWiYATmCP8k9)
   1. Within minutes, you should receive an email to your inbox with a PDF visa letter attached.

If you are a **contractor**, please follow the process below.
   1. Fill out this [form](https://forms.gle/uNViiMt1A6h6Q6SK8)
   1. Within minutes, you should receive an email to your inbox with a PDF visa letter attached.

If you are **employed via a PEO**, you will need to reach out to your PEO to receive this letter. All PEOs have been given a template for them to use for this purpose.

If you have any further questions regarding a visa invitation letter, please reach out in `#people-connect`.
